import absoftware.domain.Event;
import absoftware.domain.EventName;
import absoftware.service.PriorityQueue;
import absoftware.service.PriorityQueueService;

public class Main {

    public static void main(String[] args) {
        PriorityQueue<Event> eventPriorityQueue = new PriorityQueue<>();
        PriorityQueueService priorityQueueService = new PriorityQueueService(eventPriorityQueue);

        System.out.println("People are inserted in a priority queue at the hospital");

        System.out.println(String.format("Person with %s arrive at the hospital and priority is set to %s",
                EventName.BROKEN_LEG.computeName(), EventName.BROKEN_LEG.getPriority()));
        priorityQueueService.add(EventName.BROKEN_LEG, EventName.BROKEN_LEG.getPriority());

        System.out.println(String.format("Person with %s arrive at the hospital and priority is set to %s",
                EventName.GUNSHOT_WOUND.computeName(), EventName.GUNSHOT_WOUND.getPriority()));
        priorityQueueService.add(EventName.GUNSHOT_WOUND, EventName.GUNSHOT_WOUND.getPriority());

        System.out.println(String.format("Person with %s arrive at the hospital and priority is set to %s",
                EventName.HEADACHE.computeName(), EventName.HEADACHE.getPriority()));
        priorityQueueService.add(EventName.HEADACHE, EventName.HEADACHE.getPriority());

        System.out.println(String.format("Person with %s arrive at the hospital and priority is set to %s",
                EventName.HEART_ATTACK.computeName(), EventName.HEART_ATTACK.getPriority()));
        priorityQueueService.add(EventName.HEART_ATTACK, EventName.HEART_ATTACK.getPriority());

        System.out.println(String.format("Person with %s arrive at the hospital and priority is set to %s",
                EventName.PAPER_CUT.computeName(), EventName.PAPER_CUT.getPriority()));
        priorityQueueService.add(EventName.PAPER_CUT, EventName.PAPER_CUT.getPriority());

        System.out.println(String.format("Retrieve the person with the max priority: %s",
                priorityQueueService.peek().getEventName()));

        System.out.println("A person with bigger priority is added in line");
        priorityQueueService.add(EventName.ALMOST_DEAD, EventName.ALMOST_DEAD.getPriority());

        System.out.println(String.format("Retrieve the person with the max priority: %s",
                priorityQueueService.peek().getEventName()));

        System.out.println(String.format("Process the person with the max priority: %s",
                priorityQueueService.poll().getEventName()));

        System.out.println(
                String.format("Person with %s from the line is going home", EventName.PAPER_CUT.computeName()));
        priorityQueueService.remove(EventName.PAPER_CUT, EventName.PAPER_CUT.getPriority());

        System.out.println("Process all person in priority order");
        for (Object object : priorityQueueService.processEvents()) {
            Event event = (Event) object;
            System.out.println(String.format("Person with %s is entering in the doctor office",
                    ((EventName) event.getEventName()).computeName()));
        }
    }
}
