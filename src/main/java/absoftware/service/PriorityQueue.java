package absoftware.service;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Elements from the array are retrieved order by their comparator or by the comparator given in the constructor.
 * <p>
 * Implementation:
 * This queue provides:
 * <p>
 * Time complexity:
 * Constant time O(1) for peek, size.
 * Logarithmic time O(log(n)) for insert, and poll.
 * Linear time O(n) for remove(Object), clear
 * <p>
 * The space complexity is O(n), n represent size of the array
 *
 * @param <T> type of the element stored in the array
 * @author Alin Bizau
 */
public class PriorityQueue<T> {
    private static final int DEFAULT_CAPACITY = 10;
    private Object[] queue;
    private final int capacity;
    private int size;
    private final Comparator<T> comparator;

    public PriorityQueue() {
        this(DEFAULT_CAPACITY, null);
    }

    public PriorityQueue(int initialCapacity,
                         Comparator<T> comparator) {
        if (initialCapacity < 1) {
            throw new IllegalArgumentException("Should provide an initial capacity greater than 0");
        }
        this.queue = new Object[initialCapacity];
        this.capacity = initialCapacity;
        this.size = 0;
        this.comparator = comparator;
    }

    PriorityQueue(int capacity) {
        this(capacity, null);
    }

    public PriorityQueue(Comparator<T> comparator) {
        this(DEFAULT_CAPACITY, comparator);
    }

    /**
     * Insert a new element in the heap and swap element with the parent element until the greater element is on top
     *
     * @param object to be inserted
     *
     * O(log(n)) logarithmic time, running time is proportional to the logarithm of the array size
     * insert an element at the end of the array and
     * then compare with parent (currentIndex / 2) until is smaller than parent.
     */
    @SuppressWarnings("unchecked")
    public void insert(@NotNull T object) {
        synchronized (this) {
            if (this.size >= this.capacity) {
                resize();
            }

            // add new element to the end of the queue
            this.queue[this.size] = object;
            // increase size of the queue by 1
            this.size += 1;
            // compute current index of current inserted element
            int currentIndex = this.size - 1;

            if (this.size > 1) {
                // until current is greater than parent element swap elements
                while (true) {
                    int parentIndex = computeParentIndex(currentIndex);
                    T currentElement = (T) this.queue[currentIndex];
                    T parentElement = (T) this.queue[parentIndex];
                    // compare current with parent element and if is greater than promote child node to be parent node
                    if (!compare(currentElement, parentElement) || currentIndex == 0) {
                        break;
                    }

                    // swap child with parent element
                    swap(currentIndex, parentIndex);
                    // compute new current index
                    currentIndex = parentIndex;
                }
            }
        }
    }

    /**
     * Return the peek from the queue
     *
     * @return the head element or null if queue is empty
     * <p>
     * O(1) constant time, return the first element from array, the time is the same regarding array size
     */
    @Nullable
    @SuppressWarnings("unchecked")
    public T peek() {
        synchronized (this) {
            if (this.size == 0) {
                return null;
            }

            return (T) this.queue[0];
        }
    }

    /**
     * Remove and return the root element from the queue or return null is queue is empty
     *
     * @return the head element from the queue
     * <p>
     * O(log(n)) logarithmic time, running time is proportional to the logarithm of the array size
     * compare with the parent(current element index / 2) so proportial to the logarithm of size.
     */
    @Nullable
    public T poll() {
        synchronized (this) {
            // get root node, root node is always the bigger element from a max heap
            @SuppressWarnings("unchecked")
            T result = (T) this.queue[0];
            if (result == null || this.size == 0) {
                return null;
            }

            // get last element
            Object lastElement = this.queue[size - 1];
            // Replace root with last element right node
            this.queue[0] = lastElement;
            // Decrease size of array
            this.size = this.size - 1;
            // Fix the heap
            computeMaxHeap(0);

            return result;
        }
    }

    /**
     * @param object that need to be removed
     * @return true if the element is successfully removed or false if element is not found
     * <p>
     * O(n) linear time: find the index of the object in a linear time
     * proportional to the size of the array(from 0 to n)
     */
    public boolean remove(@NotNull Object object) {
        synchronized (this) {
            // find index of the element to be removed
            int index = indexOf(object);
            if (index == -1) {
                return false;
            } else {
                this.queue[index] = null;
                // get last element
                Object lastElement = this.queue[size - 1];
                // Replace element at index with last element right node
                this.queue[index] = lastElement;
                // Decrease size of array
                this.size = this.size - 1;
                // Fix the heap
                computeMaxHeap(index);

                return true;
            }
        }
    }

    /**
     * @return size of the queue
     * <p>
     * O(1) constant time, return directly the value
     * the time is the same regarding array size
     */
    public int size() {
        synchronized (this) {
            return this.size;
        }
    }

    /**
     * Removes all of the elements from this queue.
     * The queue will be empty after this.
     * <p>
     * O(n) linear time, iterate the list from 0 to n, proportional to the array size but if size of current array is
     * bigger e.g. 100k than clear should be reset to initial capacity.
     */
    public void clear() {
        synchronized (this) {
            this.queue = new Object[this.capacity];
            this.size = 0;
        }
    }

    /**
     * Resize the array to double size
     */
    private void resize() {
        this.queue = Arrays.copyOf(this.queue, 2 * this.queue.length);
    }

    /**
     * Compare two objects from queue, using comparator if defined in the constructor or using object comparator
     *
     * @param firstElement  to be compared
     * @param secondElement to be compared
     * @return the comparison result
     */
    @SuppressWarnings("unchecked")
    private boolean compare(T firstElement, T secondElement) {
        if (comparator != null) {
            return comparator.compare(firstElement, secondElement) < 0;
        } else {
            return ((Comparable<? super T>) firstElement).compareTo(secondElement) < 0;
        }
    }

    /**
     * Swap the current with parent element
     *
     * @param currentIndex of the element
     * @param parentIndex  of the element
     */
    private void swap(int currentIndex, int parentIndex) {
        if (currentIndex < 0 || currentIndex > this.queue.length - 1
                || parentIndex < 0 || parentIndex > this.queue.length - 1) {
            throw new IllegalArgumentException("Indexes are outside the queue");
        }
        Object tmp = this.queue[currentIndex];
        this.queue[currentIndex] = this.queue[parentIndex];
        this.queue[parentIndex] = tmp;
    }

    @SuppressWarnings("unchecked")
    private void computeMaxHeap(int index) {
        if (isLeaf(index)) {
            return;
        }

        // max element index is initialize as root
        int maxElementIndex = index;
        //compute left child index
        int leftChildIndex = computeLeftChildIndex(index);
        //compute right child index
        int rightChildIndex = computeRightChildIndex(index);

        T rootElement = (T) this.queue[maxElementIndex];
        T leftElement = (T) this.queue[leftChildIndex];
        T rightElement = (T) this.queue[rightChildIndex];

        /* compare if left element is greater than root element or right element is grater than root element
         * then compare is left element is greater than right element
         * then new max element index is left child index else right child index */
        if ((leftChildIndex < this.size && compare(leftElement, rootElement)) ||
                (rightChildIndex < this.size && compare(rightElement, rootElement))) {
            if (compare(leftElement, rightElement)) {
                maxElementIndex = leftChildIndex;
            } else {
                maxElementIndex = rightChildIndex;
            }
        }

        // if the new max element index is different that the old max element index than swap the elements from queue
        if (maxElementIndex != index) {
            swap(index, maxElementIndex);
            // repeat this steps going recursively
            computeMaxHeap(maxElementIndex);
        }
    }

    /**
     * Find the index of a given object from the queue
     *
     * @param object to find the index
     * @return index is is found or -1 if not
     */
    private int indexOf(Object object) {
        if (object != null) {
            for (int i = 0; i < this.size; i++) {
                if (object.equals(this.queue[i])) {
                    return i;
                }
            }
        }

        return -1;
    }

    /**
     * @param index of the current element
     * @return the parent index
     */
    private int computeParentIndex(int index) {
        return index / 2;
    }

    /**
     * @param index of the current element
     * @return the left child index
     */
    private int computeLeftChildIndex(int index) {
        return 2 * index + 1;
    }

    /**
     * @param index of the current element
     * @return the right child index
     */
    private int computeRightChildIndex(int index) {
        return 2 * index + 2;
    }

    /**
     * @param index of the current element
     * @return true if the element is a leaf, false if not
     */
    private boolean isLeaf(int index) {
        return index >= (this.size / 2) && index <= this.size;
    }
}
