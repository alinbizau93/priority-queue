package absoftware.service;

import absoftware.domain.Event;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PriorityQueueService<T> implements PriorityQueueInterface<T> {
    private final PriorityQueue<Event> priorityQueue;

    public PriorityQueueService(PriorityQueue<Event> priorityQueue) {
        this.priorityQueue = priorityQueue;
    }

    @Override
    public void add(Object eventName, int priority) {
        this.priorityQueue.insert(new Event<>(eventName, priority));
    }

    @Override
    public Event peek() {
        return Optional
                .ofNullable(this.priorityQueue.peek())
                .orElseThrow(() -> new IllegalStateException(
                        "The queue is empty for now. Cannot retrieve peek element from an empty queue."));
    }

    @Override
    public void remove(Object eventName, int priority) {
        if (this.priorityQueue.size() == 0) {
            throw new IllegalStateException(
                    "The queue is empty for now. Cannot remove elements from an empty queue.");
        }

        this.priorityQueue.remove(new Event<>(eventName, priority));
    }

    @Override
    public Event poll() {
        if (this.priorityQueue.size() == 0) {
            throw new IllegalStateException(
                    "The queue is empty for now.");
        }

        return this.priorityQueue.poll();
    }

    @Override
    public List<Event> processEvents() {
        List<Event> events = new ArrayList<>();
        if (this.priorityQueue.size() == 0) {
            throw new IllegalStateException(
                    "Queue is empty");
        }

        int size = this.priorityQueue.size();
        for (int i = 0; i <= size - 1; i++) {
            events.add(this.priorityQueue.poll());
        }

        return events;
    }
}
