package absoftware.service;

import absoftware.domain.Event;

import java.util.List;

public interface PriorityQueueInterface<T> {
    void add(T eventName, int priority);

    Event peek();

    void remove(Object eventName, int priority);

    Event poll();

    List<Event> processEvents();
}
