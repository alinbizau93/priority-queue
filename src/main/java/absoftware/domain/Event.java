package absoftware.domain;

import java.util.Objects;

public class Event<T> implements Comparable<Event> {
    private T eventName;
    private int priority;

    public Event(T eventName, int priority) {
        this.eventName = eventName;
        this.priority = priority;
    }

    public Event(int priority) {
        this.priority = priority;
    }

    public Event() {
    }

    public T getEventName() {
        return eventName;
    }

    public void setEventName(T eventName) {
        this.eventName = eventName;
    }

    private int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Event event = (Event) o;
        return priority == event.priority &&
                eventName == event.eventName;
    }

    @Override
    public int hashCode() {
        return Objects.hash(eventName, priority);
    }

    @Override
    public int compareTo(Event event) {
        if (this.getPriority() < event.getPriority()) {
            return 1;
        } else if (this.getPriority() > event.getPriority()) {
            return -1;
        }

        return 0;
    }

    @Override
    public String toString() {
        return "Event{" +
                "eventName=" + eventName +
                ", priority=" + priority +
                '}';
    }
}
