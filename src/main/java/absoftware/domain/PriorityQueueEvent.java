package absoftware.domain;

public interface PriorityQueueEvent {

    String computeName();

    default int getPriority() {
        return 0;
    }
}
