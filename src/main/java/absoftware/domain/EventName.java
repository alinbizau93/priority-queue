package absoftware.domain;

public enum EventName implements PriorityQueueEvent {
    BROKEN_LEG {
        @Override
        public String computeName() {
            return "Broken leg";
        }

        @Override
        public int getPriority() {
            return 30;
        }
    },
    HEART_ATTACK {
        @Override
        public String computeName() {
            return "Heart attack";
        }

        @Override
        public int getPriority() {
            return 100;
        }
    },
    GUNSHOT_WOUND {
        @Override
        public String computeName() {
            return "Gunshot wound";
        }

        @Override
        public int getPriority() {
            return 50;
        }
    },
    PAPER_CUT {
        @Override
        public String computeName() {
            return "Paper cut";
        }

        @Override
        public int getPriority() {
            return 10;
        }
    },
    HEADACHE {
        @Override
        public String computeName() {
            return "Headache";
        }
    },
    ALMOST_DEAD {
        @Override
        public String computeName() {
            return "Almost dead";
        }

        @Override
        public int getPriority() {
            return 1000;
        }
    }
}
