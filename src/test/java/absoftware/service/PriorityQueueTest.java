package absoftware.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class PriorityQueueTest {
    private final PriorityQueue<PriorityQueueItem> priorityQueue = new PriorityQueue<>();

    @BeforeEach
    void setUp() {
        priorityQueue.insert(new PriorityQueueItem(30));
        priorityQueue.insert(new PriorityQueueItem(50));
        priorityQueue.insert(new PriorityQueueItem(0));
        priorityQueue.insert(new PriorityQueueItem(100));
        priorityQueue.insert(new PriorityQueueItem(10));
        priorityQueue.insert(new PriorityQueueItem(70));
        priorityQueue.insert(new PriorityQueueItem(60));
        priorityQueue.insert(new PriorityQueueItem(40));
        priorityQueue.insert(new PriorityQueueItem(35));
        priorityQueue.insert(new PriorityQueueItem(5));
    }

    @AfterEach
    void tearDown() {
        priorityQueue.clear();
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenCreatingQueueWithCapacityZero() {
        assertThrows(IllegalArgumentException.class, () -> new PriorityQueue<>(0));
    }

    @Test
    void shouldDoubleArraySizeIfTheQueueIsFullWhenInserting() {
        priorityQueue.insert(new PriorityQueueItem(30));
        assertEquals(11, priorityQueue.size());
    }

    @Test
    void ShouldReturnPeekElementIfQueueIsNotEmpty() {
        PriorityQueueItem peek = priorityQueue.peek();
        assertEquals(100, peek.getPriority());
    }

    @Test
    void ShouldReturnNullElementIfQueueIsEmpty() {
        final PriorityQueue<PriorityQueueItem> queue = new PriorityQueue<>();
        assertNull(queue.peek());
    }

    @Test
    void shouldReturnAndDeleteTheRootElementIfQueueIsNotEmpty() {
        assertEquals(100, priorityQueue.poll().getPriority());
        assertEquals(70, priorityQueue.poll().getPriority());
        assertEquals(60, priorityQueue.poll().getPriority());
        assertEquals(50, priorityQueue.poll().getPriority());
        assertEquals(40, priorityQueue.poll().getPriority());
        assertEquals(35, priorityQueue.poll().getPriority());
        assertEquals(30, priorityQueue.poll().getPriority());
        assertEquals(10, priorityQueue.poll().getPriority());
        assertEquals(5, priorityQueue.poll().getPriority());
        assertEquals(0, priorityQueue.poll().getPriority());
        assertNull(priorityQueue.poll());
    }

    @Test
    void shouldReturnAndDeleteTheRootElementIfQueueIsNotEmptyWithCustomComparator() {
        final PriorityQueue<PriorityQueueItem> queue = new PriorityQueue<>(
                (o1, o2) -> {
                    if (o1.getPriority() > o2.getPriority()) {
                        return 1;
                    } else if (o1.getPriority() < o2.getPriority()) {
                        return -1;
                    }

                    return 0;
                });

        queue.insert(new PriorityQueueItem(30));
        queue.insert(new PriorityQueueItem(50));
        queue.insert(new PriorityQueueItem(0));
        queue.insert(new PriorityQueueItem(100));
        queue.insert(new PriorityQueueItem(10));

        assertEquals(0, queue.poll().getPriority());
        assertEquals(10, queue.poll().getPriority());
        assertEquals(30, queue.poll().getPriority());
        assertEquals(50, queue.poll().getPriority());
        assertEquals(100, queue.poll().getPriority());
        assertNull(queue.poll());
    }

    @Test
    void shouldReturnNullIfQueueIsEmpty() {
        final PriorityQueue<PriorityQueueItem> queue = new PriorityQueue<>();
        assertNull(queue.poll());
    }

    @Test
    void shouldRemoveElementIfQueueIsNotEmpty() {
        final PriorityQueue<PriorityQueueItem> queue = new PriorityQueue<>();
        queue.insert(new PriorityQueueItem(30));
        queue.insert(new PriorityQueueItem(50));
        queue.insert(new PriorityQueueItem(0));
        queue.insert(new PriorityQueueItem(100));
        queue.insert(new PriorityQueueItem(10));

        queue.remove(new PriorityQueueItem(50));
        assertEquals(4, queue.size());

        assertEquals(100, queue.poll().getPriority());
        assertEquals(30, queue.poll().getPriority());
        assertEquals(10, queue.poll().getPriority());
        assertEquals(0, queue.poll().getPriority());
        assertNull(queue.poll());
    }

    @Test
    void shouldReturnFalseIfElementIsNotFound() {
        final PriorityQueue<PriorityQueueItem> queue = new PriorityQueue<>();
        assertFalse(queue.remove(new PriorityQueueItem(50)));
    }

    @Test
    void shouldReturnTheSizeOfTheQueue() {
        assertEquals(10, priorityQueue.size());
    }

    @Test
    void shouldReturn0IfQueueIsEmpty() {
        final PriorityQueue<PriorityQueueItem> queue = new PriorityQueue<>();
        assertEquals(0, queue.size());
    }

    @Test
    void clear() {
        priorityQueue.clear();
        assertEquals(0, priorityQueue.size());
    }

    private class PriorityQueueItem implements Comparable<PriorityQueueItem> {
        private int priority;

        public PriorityQueueItem(int priority) {
            this.priority = priority;
        }

        public int getPriority() {
            return priority;
        }

        public void setPriority(int priority) {
            this.priority = priority;
        }

        @Override
        public String toString() {
            return "PriorityQueueItem{" +
                    "priority=" + priority +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            PriorityQueueItem that = (PriorityQueueItem) o;
            return priority == that.priority;
        }

        @Override
        public int hashCode() {
            return Objects.hash(priority);
        }

        @Override
        public int compareTo(PriorityQueueItem o) {
            if (this.getPriority() < o.getPriority()) {
                return 1;
            } else if (this.getPriority() > o.getPriority()) {
                return -1;
            }

            return 0;
        }
    }
}
